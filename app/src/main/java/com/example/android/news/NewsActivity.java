package com.example.android.news;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;


import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends AppCompatActivity
//implements LoaderManager<List<News>>
{
    // Adapter for the list view of the news
    private NewsAdapter mAdapter;
    /** Tag for log messages */
    public static final String LOG_TAG = NewsActivity.class.getName();
    // News query url
    private static final String NEWS_REQUEST_URL = "http://content.guardianapis.com/search";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        // Find a reference to the GridView in the layout
        GridView newsGridView = (GridView) findViewById(R.id.list);
        // Create a new adapter that takes an empty list of earthquakes as input
        mAdapter = new NewsAdapter(this, new ArrayList<News>());


        // Set the adapter on the newsGridView
        //so the list can be displayed in the user interface
        newsGridView.setAdapter(mAdapter);
    }
}
