package com.example.android.news;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NewsAdapter extends ArrayAdapter<News> {
    private Context context;
    public NewsAdapter(Activity context, ArrayList<News> stories){
        super(context, 0, stories);
    }
    /**
     * Provides a view for an AdapterView (ListView, GridView, etc.)
     *
     * @param position    The position in the list of data that should be displayed in the
     *                    list item view.
     * @param convertView The recycled view to populate.
     * @param parent      The parent ViewGroup that is used for inflation.
     * @return The View for the position in the AdapterView.
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // Check if there is an existing view list item (called convertView) that we can reuse,
        // otherwise , if convertView is null, then inflate a new new list_item layout
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        // Find the earthquake at the given position in thelist of earthquakes
        final News currentStory = getItem(position);
        // Get the title data
        String title = currentStory.getmTitle();
        // Get the section name data
        String section = currentStory.getmSection();
        // Get the author name data
        String author = "";
        // Find TextView in the layout list_item.xml with the view id art_title
        TextView titleView = (TextView) listItemView.findViewById(R.id.art_title);
        // Display the title of current news in that TextView
        titleView.setText(title);

        // Find TextView in the layout list_item.xml with the view id art_section
        TextView sectionView = (TextView) listItemView.findViewById(R.id.art_section);
        // Display the section name of current news in that TextView
        sectionView.setText(section);
        // Find TextView in the layout list_item.xml with the view id art_author
        TextView authorView = (TextView) listItemView.findViewById(R.id.art_author);
        // Display the author of current news in that TextView
        authorView.setText(author);

        // Create a new TimeInMilliseconds object from the time in milliseconds of the news
        Date dateObject = new Date(currentStory.getmDate());
        /** Create a DATE from TimeInMilliseconds dateObject **/
        // Find the TextView in the list_item.xml layout with the ID art_date
        TextView dateTextView = (TextView) listItemView.findViewById(R.id.art_date);
        // Format the date string (i.e. "Mar 3, 1984")
        String formattedDate = formatDate(dateObject);
        // Display the date of the current news in that TextView
        dateTextView.setText(formattedDate);
        /** Create a TIME from TimeInMilliseconds dateObject **/
        // Find the TextView with view ID art_time
        TextView timeView = (TextView) listItemView.findViewById(R.id.art_time);
        // Format the time string (i.e. "4:30PM")
        String formattedTime = formatTime(dateObject);
        // Display the time of the current news in that TextView
        timeView.setText(formattedTime);

        // Return the whole list item layout (containing 5 TextViews)
        // that is showing apprpriate data in the ListView
        return listItemView;
    }

    /**
     * Return the formatted date string (i.e. "Mar 3, 1984") from a Date object.
     */
    private String formatDate(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }
    /**
     * Return the formatted date string (i.e. "4:30 PM") from a Date object.
     */
    private String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        return timeFormat.format(dateObject);
    }
}
