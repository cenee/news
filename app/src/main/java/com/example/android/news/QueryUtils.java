package com.example.android.news;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.example.android.news.NewsActivity.LOG_TAG;

/**
 * Helper methods related to requesting and receiving news data from theguardian.com
 */

public final class QueryUtils {

    /**
     * Create a private constructor because no one should ever create a {@link QueryUtils} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */
    private QueryUtils() {

    }
    /**
     * Return a list of {@link News} objects that has been built up from
     * parsing the given JSON response.
     */
    private static List<News> extractFeatureFromJson(String newsJSON) {

        // If theJSON string is empty or null, then return early.
//        if (TextUtils.isEmpty(newsJSON)) {
//            return null;
//        }
        // Create an empty ArrayList that we can start adding news to
        List<News> stories = new ArrayList<>();

        // Try to parse the JSON response string. If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // Create JSON Object from JSON response string
            JSONObject root = new JSONObject(newsJSON);
            // Check if JSON has a root object
            if (root.has("response")){
                // Extract the JSON associated with thekey called "results",
                // which represents a list of stories (news).
                JSONArray storiesArray = root.getJSONArray("results");
                // Loop trough each story to create a News object
                for (int i = 0; i < storiesArray.length(); i++){
                    // Get single story at position "i" on the stories list
                    JSONObject currentStory = storiesArray.getJSONObject(i);
                    // Extract the value for the key called  “webTitle” to get title of the article
                    String webTitle = currentStory.getString("webTitle");
                    // Extract the value for the key called  “sectionName” to get section name of the article
                    String sectionName = currentStory.getString("sectionName");
                    // Extract the value for the key called  “webPublicationDate” to get time of the article
                    long published = currentStory.getLong("webPublicationDate");
                    Date dateObject = new java.sql.Date(published);
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("MM DD, yyyy");
                    // Extract the value for the key called "author" if it is available to get author name
                    String authorName;
                    if (currentStory.has("author")){
                         authorName = currentStory.getString("author");
                    } else {  authorName = "Author N/A"; }

                    // Create new News object with title, sectionName
                    // (and if it's available data: published, author)
                    // from the JSON response
                    News news = new News( webTitle, sectionName, published, authorName);
                    //Add new story to the list of stories
                    stories.add(news);
                }

            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e("QueryUtils", "Problem parsing the earthquake JSON results", e);
        }
        // Return the list of stories
        return stories;
    }

    /**
     * Returns new URL object from the given string URL.
     */
    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }
    /**
     * Query theguardian dataset and return an {@link News} object to represent a single story.
     */
    public static List<News> fetchEarthquakeData(String requestUrl) {
        //test for slow internet connection:
        //        try {
        //            Thread.sleep(2000);
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        }

        // Create URL object
        URL url = createUrl(requestUrl);

        // Perform HTTP request to the URL and receive a JSON response back
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        // Extract relevant fields from the JSON response and create a list of stories
        List<News> stories = extractFeatureFromJson(jsonResponse);

        // Return the list of News
        return stories;
    }
    /**
     * Make an HTTP request to the given URL and return a String as the response.
     */
    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        // If the URL is null, then return early.
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            int jakiKod =  urlConnection.getResponseCode();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
            Log.e(LOG_TAG, e.toString());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }
    /**
     * Convert the {@link InputStream} into a String which contains the
     * whole JSON response from the server.
     */
    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

}
