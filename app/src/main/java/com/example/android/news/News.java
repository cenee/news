package com.example.android.news;

import static android.R.attr.author;

// Object of this class is a story news
public class News {
    private String mTitle;
    private String mSection;
    private long mDate;
    private String mAuthor;


    // News class constructors new News object creation.
    /*
    * @param title is the title of the article
    * @param section is the name of the section the article belongs to
    * @param timeInMilliseconds is the publication date
    * @param author is the author name of the article
    */
    public News(String title, String section, long timeInMilliseconds, String author) {
        mTitle = title;
        mSection = section;
        mDate = timeInMilliseconds;
        mAuthor = author;
    }

    // News class constructors new News object creation.
    /*
    * @param title is the title of the article
    * @param section is the name of the section the article belongs to
    * @param author is the author name of the article
    */
    public News(String title, String section, String author) {
        mTitle = title;
        mSection = section;
        mAuthor = author;
    }

    // News class constructors new News object creation.
    /*
    * @param title is the title of the article
    * @param section is the name of the section the article belongs to
    * @param timeInMilliseconds is the publication date
    * @param author is the author name of the article
    */
    public News(String title, String section, long timeInMilliseconds) {
        mTitle = title;
        mSection = section;
        mDate = timeInMilliseconds;
    }

    // News class constructors new News object creation.
    /*
    * @param title is the title of the article
    * @param section is the name of the section the article belongs to
    */
    public News(String title, String section) {
        mTitle = title;
        mSection = section;
    }

    // Returns the title of the article
    public String getmTitle() {
        return mTitle;
    }

    // Returns the section name of the article
    public String getmSection() {
        return mSection;
    }

    // Returns the publication date of the article in miliseconds
    public long getmDate() {
        return mDate;
    }

    // Returns the author name of the article
    public String getmAuthor() {
        return mAuthor;
    }
}
